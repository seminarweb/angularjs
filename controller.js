var app = angular.module('myApp', []); //#7
console.log("Controller geladen");

app.controller('myCtrl', function ($scope, $http, $location, $log) {

    //Location Service:
    //var url = "http://" + $location.host() + ":" + $location.port(); //#6

    //HTTP Service mit AJAX Aufruf:
    $http.get("./model.json") //#6
        .then(function (response) {
            $scope.teilnehmer = response.data;
        });

    //Sortierfunktion:
    $scope.orderByMe = function (x) { //#8
        $log.log("orderByMe: " + x); //#6
        $scope.myOrderBy = x;
    }

    //Daten hinzufügen:
    $scope.addItem = function () { //#9
        $log.log("addItem: " + $scope.user.name);
        $scope.teilnehmer.push({ //Man könnte auch nur das Objekt $scope.user pushen.
            "Name": $scope.user.name,
            "Email": $scope.user.email,
            "Projekt": $scope.user.projekt
        });
        //Resetfunktion in Billig:
        $scope.user.name = "";
        $scope.user.email = "";
        $scope.user.projekt = "";
    }

    //Daten löschen:
    $scope.removeItem = function (x) {
        console.log("removeItem: " + x);
        $scope.teilnehmer.splice(x, 1);
    }

    //Eingabe Zurücksetzen:
    $scope.master = {
        name: "",
        email: "",
        projekt: ""
    };
    $scope.reset = function () {
        console.log("reset");
        $scope.user = angular.copy($scope.master);
    };
    $scope.reset();


});
